import React from "react";
import { APIProvider } from "./services/concepts-api";
import Navigation from "./services/navigation";

const App = () => {
  return (
    <APIProvider>
      <Navigation />
    </APIProvider>
  );
};

export default App;
