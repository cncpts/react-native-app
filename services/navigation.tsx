import * as React from "react";
import { Button, Image, TouchableOpacity } from "react-native";
import { NavigationContainer, useNavigation } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import UrlsBrowserScreen from "../features/tagging/UrlBrowserScreen";
import TaggingScreen from "../features/tagging/TaggingScreen";
import ConceptsScreen from "../features/concepts/ConceptsScreen";
import SignInScreen from "../features/authentication/SignInScreen";
import ProfileScreen from "../features/authentication/ProfileScreen";

const LogoTitle = () => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity onPress={() => navigation.navigate("Tag")}>
      <Image
        style={{ width: 52, height: 40, resizeMode: "stretch" }}
        source={require("../assets/logo.png")}
      />
    </TouchableOpacity>
  );
};

const LoginNav = () => {
  const navigation = useNavigation();
  return <Button onPress={() => navigation.navigate("SignIn")} title="Login" />;
};

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const TabRoot = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Tag" component={UrlsBrowserScreen} />
      <Tab.Screen name="Concepts" component={ConceptsScreen} />
      <Tab.Screen name="Profile" component={ProfileScreen} />
    </Tab.Navigator>
  );
};

export default () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="TabRoot"
        screenOptions={{
          headerStyle: {
            backgroundColor: "#fff",
          },
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
          },
          headerTitle: () => <LogoTitle />,
          headerRight: () => <LoginNav />,
        }}
      >
        <Stack.Screen name="TabRoot" component={TabRoot} />
        <Stack.Screen name="Tagging" component={TaggingScreen} />
        <Stack.Screen name="SignIn" component={SignInScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
