import * as React from "react";
import * as R from "ramda";
import { FlatList, ActivityIndicator } from "react-native";
import { ListItem, SearchBar } from "react-native-elements";
import gql from "graphql-tag";
import { useApolloClient } from "@apollo/react-hooks";
import { createFeedContext, createApolloHook } from "productive-feed";

const SEARCH_CONCEPTS = gql`
  query SearchConcepts($name: String, $limit: Int, $offset: Int) {
    data: concepts(
      where: { name: { _ilike: $name } }
      limit: $limit
      offset: $offset
      order_by: { id: asc }
    ) {
      id
      name
    }
  }
`;

const [ConceptsProvider, useConcepts] = createFeedContext();

export default () => {
  return (
    <ConceptsProvider
      useFetch={createApolloHook(useApolloClient, SEARCH_CONCEPTS)}
      options={{ defaults: { pageSize: 50 } }}
    >
      <ConceptSearchBar />
      <ConceptsScreen />
    </ConceptsProvider>
  );
};

const ConceptSearchBar = () => {
  const { setFilter, filters, resetFilters } = useConcepts();

  const handleTextChange = (input) => {
    return R.unless(R.isNil, (input) => setFilter("name", `%${input}%`))(input);
  };

  return (
    <SearchBar
      placeholder="Type Here..."
      onChangeText={handleTextChange}
      value={R.slice(1, -1, filters.name || "")}
      onClear={resetFilters}
    />
  );
};

const ConceptsScreen = () => {
  const { data, loading, nextPage } = useConcepts();
  if (!data && loading) return <ActivityIndicator size="large" />;
  return (
    <FlatList
      onEndReached={nextPage}
      data={data}
      renderItem={({ item }) => (
        <ListItem bottomDivider>
          <ListItem.Content>
            <ListItem.Title>{item.name}</ListItem.Title>
          </ListItem.Content>
        </ListItem>
      )}
      keyExtractor={(item) => item.id.toString()}
    />
  );
};
