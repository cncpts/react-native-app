import React, { useState } from "react";
import { View, Text } from "react-native";
import { Input, Button, ButtonGroup } from "react-native-elements";
import { assoc } from "ramda";
import { useNavigation } from "@react-navigation/native";

const SignInScreen = () => {
  const { authenticated, login, register, loading } = {
    authenticated: false,
    loading: false,
    login: (_: any) => {
      alert("Login");
      return Promise.resolve();
    },
    register: (_: any) => {
      alert("Register");
      return Promise.resolve();
    },
  }; // TODO: change to useAuth()

  const navigation = useNavigation();
  const [selectedForm, setSelectedForm] = useState(0);
  const [userCredentials, setUserCredentials] = useState({
    email: "",
    password: "",
    confirmPassword: "",
  });
  const buttons = ["Login", "Register"];

  const handleLogin = () => {
    login(userCredentials).then(() => navigation.navigate("Profile"));
  };

  const handleRegister = () => {
    if (userCredentials.password !== userCredentials.confirmPassword) return;
    register(userCredentials).then(() => navigation.navigate("Profile"));
  };

  if (authenticated) return <Text>You Are Already Logged In</Text>;
  if (loading) return <Text>Please Wait...</Text>;
  return (
    <SignInForm
      selectedForm={selectedForm}
      setSelectedForm={setSelectedForm}
      buttons={buttons}
      userCredentials={userCredentials}
      setUserCredentials={setUserCredentials}
      handlers={{ handleRegister, handleLogin }}
    />
  );
};

const SignInForm = ({
  selectedForm,
  setSelectedForm,
  buttons,
  userCredentials,
  setUserCredentials,
  handlers,
}) => (
  <View>
    <ButtonGroup
      onPress={setSelectedForm}
      selectedIndex={selectedForm}
      buttons={buttons}
    />
    <Input
      value={userCredentials.email}
      onChangeText={(text) => setUserCredentials(assoc("email", text))}
      placeholder="email@address.com"
      leftIconContainerStyle={{ marginRight: 5 }}
      leftIcon={{ type: "material", name: "mail" }}
      autoCapitalize="none"
    />
    <Input
      value={userCredentials.password}
      onChangeText={(text) => setUserCredentials(assoc("password", text))}
      placeholder="password"
      leftIcon={{ type: "material", name: "lock" }}
      leftIconContainerStyle={{ marginRight: 5 }}
      secureTextEntry
      autoCapitalize="none"
    />
    {selectedForm === 1 ? (
      <Input
        value={userCredentials.confirmPassword}
        onChangeText={(text) =>
          setUserCredentials(assoc("confirmPassword", text))
        }
        placeholder="confirm password"
        leftIcon={{ type: "material", name: "lock" }}
        leftIconContainerStyle={{ marginRight: 5 }}
        secureTextEntry
        autoCapitalize="none"
      />
    ) : null}
    <Button
      title={selectedForm === 0 ? "Login" : "Register"}
      type="outline"
      onPress={
        selectedForm === 0 ? handlers.handleLogin : handlers.handleRegister
      }
    />
  </View>
);

export default SignInScreen;
