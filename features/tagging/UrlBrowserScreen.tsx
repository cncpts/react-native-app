import React, { useState } from "react";
import { Text, View, StyleSheet } from "react-native";
import { Dimensions, Animated, RefreshControl } from "react-native";
import { SwipeListView } from "react-native-swipe-list-view";
import { ListItem, Overlay, Input, Button } from "react-native-elements";
import { useNavigation } from "@react-navigation/native";
import gql from "graphql-tag";
import * as R from "ramda";
import { useMutation, useSubscription } from "@apollo/react-hooks";
import { createFeedContext } from "productive-feed";

const FETCH_URIS = gql`
  subscription Subscription($limit: Int, $offset: Int) {
    data: pending_process_resources_to_tag(
      limit: $limit
      offset: $offset
      order_by: { duration: desc }
    ) {
      duration
      id
      last_visted
      uri
    }
  }
`;

export const [UrisProvider, useUris] = createFeedContext();
export default () => {
  return <UrisBrowser />;
};

const UrisBrowser = () => {
  const navigation = useNavigation();
  const { data, loading } = useSubscription(FETCH_URIS, {
    variables: { offset: 0, limit: 100 },
  });

  const [rowToDelete, setRowToDelete] = useState(null);

  const renderData = data?.data || [];
  const rowTranslateAnimatedValues = R.zipObj(
    renderData.map((r) => r.id),
    renderData.map((r) => new Animated.Value(1))
  );
  return (
    <>
      {rowToDelete && (
        <IgnoreUrlModal
          entry={rowToDelete}
          closeOverlay={() => setRowToDelete(null)}
        />
      )}
      <SwipeListView
        disableRightSwipe
        data={renderData}
        renderItem={renderItem(navigation)}
        renderHiddenItem={renderHiddenItem}
        rightOpenValue={-Dimensions.get("window").width}
        previewRowKey={"0"}
        previewOpenValue={-40}
        previewOpenDelay={1000}
        onSwipeValueChange={onSwipeValueChange({
          rowTranslateAnimatedValues,
          setRowToDelete: (id) => {
            setRowToDelete(renderData.find((entry) => entry.id == id));
          },
        })}
        removeClippedSubviews={true}
        keyExtractor={({ id }) => id.toString()}
        refreshControl={<RefreshControl refreshing={loading} />}
      />
    </>
  );
};

const renderItem = (navigation) => (rowData, _rowMap) => {
  return (
    <ListItem
      onPress={() => {
        navigation.navigate("Tagging", rowData.item);
      }}
      bottomDivider
    >
      <ListItem.Content>
        <ListItem.Title>{rowData.item.uri}</ListItem.Title>
      </ListItem.Content>
    </ListItem>
  );
};

const renderHiddenItem = () => (
  <View style={styles.rowBack}>
    <View style={[styles.backRightBtn, styles.backRightBtnRight]}>
      <Text style={styles.backTextWhite}>Delete</Text>
    </View>
  </View>
);

const IGNORE_URI = gql`
  mutation MyMutation($uri_regex: String!) {
    insert_pending_process_ignored_resources_one(
      object: { uri_regex: $uri_regex }
    ) {
      id
    }
  }
`;

const IgnoreUrlModal = ({ entry, closeOverlay }) => {
  const [ignoreUri] = useMutation(IGNORE_URI);
  const [uri, setUri] = useState(entry.uri);
  return (
    <Overlay
      isVisible={true}
      fullScreen={true}
      onBackdropPress={closeOverlay}
      // overlayStyle={{
      //   flex: 0.5,
      //   justifyContent: "space-between",
      //   width: "90%",
      // }}
    >
      <>
        <Input value={uri} onChangeText={setUri} multiline />
        <View style={{ marginBottom: 20 }}>
          <Button
            title="Ignore exact URI"
            type="outline"
            onPress={() =>
              ignoreUri({ variables: { uri_regex: uri } }).then(closeOverlay)
            }
          />
        </View>
        <View style={{ marginBottom: 64 }}>
          <Button
            title="Ignore all sub-URIs"
            type="outline"
            onPress={() =>
              ignoreUri({ variables: { uri_regex: `${uri}%` } }).then(
                closeOverlay
              )
            }
          />
        </View>
        <Button title="Cancel" type="outline" onPress={closeOverlay} />
      </>
    </Overlay>
  );
};

const onSwipeValueChange = ({ rowTranslateAnimatedValues, setRowToDelete }) => (
  swipeData
) => {
  const { key, value } = swipeData;
  if (value < -Dimensions.get("window").width) {
    Animated.timing(rowTranslateAnimatedValues[key], {
      useNativeDriver: true,
      toValue: 0,
      duration: 200,
    }).start(() => {
      // pop modal with url
      setRowToDelete(key);
    });
  }
};

const styles = StyleSheet.create({
  backTextWhite: {
    color: "#FFF",
  },
  rowBack: {
    alignItems: "center",
    backgroundColor: "red",
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: 15,
  },
  backRightBtn: {
    alignItems: "center",
    bottom: 0,
    justifyContent: "center",
    position: "absolute",
    top: 0,
    width: 75,
  },
  backRightBtnRight: {
    backgroundColor: "red",
    right: 0,
  },
});
