import React, { useState } from "react";
import { WebView } from "react-native-webview";
import { View, Text, ScrollView, ActivityIndicator } from "react-native";
import { createFeedContext, createFetchHook } from "productive-feed";
import { ListItem, Overlay, Input, Button } from "react-native-elements";
import { FlatList } from "react-native-gesture-handler";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

import { REACT_APP_KEYWORD_ENDPOINT } from "../../env";

const TaggingScreen = ({ route }) => {
  const { uri } = route.params;
  const [tag, setTag] = useState(null);
  return (
    <View style={{ flex: 1 }}>
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        <Text>{uri}</Text>
      </ScrollView>
      <WebView containerStyle={{ flex: 80 }} source={{ uri }} />
      <View style={{ height: 3, backgroundColor: "#e1e8ee" }} />
      <View style={{ flex: 20 }}>
        <Tags uri={uri} setTag={setTag} />
      </View>
      {tag && (
        <TagUrlModal data={{ uri, tag }} closeOverlay={() => setTag(null)} />
      )}
    </View>
  );
};

const [TagsProvider, useTags] = createFeedContext();

const Tags = ({ uri, setTag }) => {
  return (
    <TagsProvider
      useFetch={createFetchHook(REACT_APP_KEYWORD_ENDPOINT, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ url: uri }),
      })}
      options={{ defaults: { pageSize: 10 }, scroll: false }}
    >
      <TagsList setTag={setTag} />
    </TagsProvider>
  );
};

const TagsList = ({ setTag }) => {
  const { loading, error, data } = useTags();

  if (!data && loading) return <ActivityIndicator size="large" />;
  if (error) return <Text>{`Error! ${error}`}</Text>;
  if (data)
    return (
      <FlatList
        data={data}
        renderItem={({ item }) => (
          <ListItem onPress={() => setTag(item[0])} bottomDivider>
            <ListItem.Content>
              <ListItem.Title>{item[0]}</ListItem.Title>
            </ListItem.Content>
          </ListItem>
        )}
        keyExtractor={(item) => item[0].toString()}
      />
    );
  return null;
};

const TAG_URL = gql`
  mutation MyMutation($concept: String!, $url: String!) {
    insert_concepts_resource_concept_tag_one(
      object: {
        concept: {
          data: { name: $concept }
          on_conflict: { update_columns: name, constraint: concepts_name_key }
        }
        resource: {
          data: { url: $url }
          on_conflict: { constraint: resources_url_key, update_columns: url }
        }
      }
      on_conflict: {
        constraint: resource_concept_tag_resource_id_concept_id_key
        update_columns: updated_at
      }
    ) {
      concept_id
      resource_id
      updated_at
    }
  }
`;

const TagUrlModal = ({ data, closeOverlay }) => {
  const [tagUrl] = useMutation(TAG_URL);
  const [uri, setUri] = useState(data.uri);
  const [tag, setTag] = useState(data.tag);
  return (
    <Overlay isVisible={true} fullScreen={true} onBackdropPress={closeOverlay}>
      <>
        <Input value={uri} onChangeText={setUri} multiline />
        <Input value={tag} onChangeText={setTag} multiline />
        <View style={{ marginBottom: 64 }}>
          <Button
            title="Tag URI"
            type="outline"
            onPress={() =>
              tagUrl({ variables: { concept: tag, url: uri } }).then(
                closeOverlay
              )
            }
          />
        </View>

        <Button title="Cancel" type="outline" onPress={closeOverlay} />
      </>
    </Overlay>
  );
};

export default TaggingScreen;
